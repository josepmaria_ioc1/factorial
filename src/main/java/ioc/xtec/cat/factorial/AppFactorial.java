package ioc.xtec.cat.factorial;

import java.util.Scanner;

/**
 * Aquest programa permet a l'usuari calcular el factorial d'un nombre de manera recursiva o sense recursivitat.
 *
 * @author Josep Maria Mirada
 * @version 1.0
 */

public class AppFactorial {
	
	
    /**
     * Funció principal que demana a l'usuari un nombre i l'opció de càlcul,
     * i imprimeix el resultat del factorial segons l'opció escollida.
     *
     * @param args Arguments de línia de comandes (no utilitzats aquí).
     */
	public static void main(String[] args) {
		
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

        System.out.print("Espero que hagueu aprés fent aquest mòdul!!!");
        System.out.print("Introdueix un nombre per calcular el factorial: ");
        int num = scanner.nextInt();

        System.out.print("Escull la forma de càlcul (1 per recursiva, 2 sense recursivitat): ");
        int opcio = scanner.nextInt();

        int resultat;

        // Utilitzar switch case per gestionar les opcions
        switch (opcio) {
            case 1:
                resultat = factorialRecursiu(num);
                break;
            case 2:
                resultat = factorialNoRecursiu(num);
                break;
            default:
                System.out.println("Opció no vàlida. Si us plau, selecciona 1 o 2.");
                return;
        }

        System.out.println("El factorial de " + num + " és: " + resultat);

	}
	
	
    /**
     * Calcula el factorial d'un nombre utilitzant recursivitat.
     *
     * @param n El nombre pel qual es calcularà el factorial.
     * @return El resultat del factorial.
     * @throws IllegalArgumentException Si el nombre és negatiu.
     */
	public static int factorialRecursiu(int n) {
		
		if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
		
        if (n == 0 || n == 1)
            return 1;
        else
            return n * factorialRecursiu(n - 1);
    }
	
	
    /**
     * Calcula el factorial d'un nombre sense utilitzar recursivitat.
     *
     * @param n El nombre pel qual es calcularà el factorial.
     * @return El resultat del factorial.
     * @throws IllegalArgumentException Si el nombre és negatiu.
     */
	public static int factorialNoRecursiu(int n) {
		
		if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
		
        int resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }

}
